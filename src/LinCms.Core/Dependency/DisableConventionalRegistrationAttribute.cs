﻿using System;

namespace LinCms.Dependency
{
    /// <summary>
    /// 禁用默认Service后缀的服务，禁用默认的接口就注入服务
    /// </summary>
    public class DisableConventionalRegistrationAttribute : Attribute
    {

    }
}
